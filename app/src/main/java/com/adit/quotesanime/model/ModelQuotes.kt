package com.adit.quotesanime.model

import java.io.Serializable

class ModelQuotes : Serializable {
    lateinit var anime: String
    lateinit var character: String
    lateinit var quote: String
}